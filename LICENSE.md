<p xmlns:dct="http://purl.org/dc/terms/">
<a rel="license" href="http://creativecommons.org/publicdomain/mark/1.0/">
<img src="https://i.creativecommons.org/p/mark/1.0/88x31.png"
     style="border-style: none;" alt="Public Domain Mark" />
</a>
<br />
This work (<span property="dct:title">Music Scores</span>, by <span resource="[_:creator]" rel="dct:creator"><span property="dct:title">Various Authors</span></span>) is free of known copyright restrictions.
</p>
